#!/usr/bin/env python
import pika
import common

parameters = pika.ConnectionParameters(host='localhost')
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
qname = common.make_qname()
message = common.make_message()

channel.basic_consume(common.ack_callback, queue=qname)

print (" [*] Waiting for messages. To exit press CTRL+C")
channel.start_consuming()
