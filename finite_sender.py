#!/usr/bin/env python
import sys
import pika
import common

parameters = pika.ConnectionParameters(host='localhost')
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
qname = common.make_qname()
message = common.make_message()
amount = int(sys.argv[1])
channel.queue_declare(queue=qname)

for i in range(amount):
    channel.basic_publish(exchange='',
                          routing_key=qname,
                          body=message)
print (" [x] Sent {0} messages with: {1}".format(amount, message))
connection.close()
