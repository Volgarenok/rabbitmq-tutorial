#!/usr/bin/env python
import pika
import common
import sys

parameters = pika.ConnectionParameters(host='localhost')
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
qname = common.make_qname()
message = common.make_message()
amount = int(sys.argv[1])

for i in range(amount):
    method, header, body = channel.basic_get(queue=qname)
    common.ack_callback(channel, method, '', body)

print (" [*] Recieved {0} messages".format(amount))

