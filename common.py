import time
import random

def make_message():
    return 'my_awesone_message'

def make_qname():
    return 'message_stream'

def simple_callback(ch, method, propertirs, body):
    print ("Recieved {0}".format(body))

def pause(messages_per_second, dispersion):
    mw_pause = 1.0 / messages_per_second
    left = mw_pause * (1.0 - dispersion)
    right = mw_pause * (1.0 + dispersion)
    return random.uniform(left, right)

def ack_callback(ch, method, propertirs, body):
    print ("Recieved {0}".format(body))
    time.sleep(pause(10, 0.1))
    ch.basic_ack(delivery_tag=method.delivery_tag)

def nack_callback(ch, method, propertirs, body):
    print ("Recieved {0} but rejected".format(body))
    time.sleep(pause(10, 0.1))
    ch.basic_nack(delivery_tag=method.delivery_tag)
